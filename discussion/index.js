let cities = [
	{"city": "Quezon City","province": "Manila", "country": "Philippines"},
	{"city": "Makati City","province": "Manila", "country": "Philippines"},
	{"city": "Manila City","province": "Manila", "country": "Philippines"}
]
console.log(cities);

let batchesArr = [{batchName: "Batch X"},{batchName: "Batch y"},];
console.log(batchesArr);
console.log("Result from stringify method");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify([{name: "Jygs",age: 23, address: {city: "Valenzuela", country: "Philippines"}}]);
// direct approach
//let data = [{name: "Jygs",age: 23, address: {city: "Valenzuela", country: "Philippines"}}];
console.log(data);
console.log(JSON.stringify(data));

let fname = prompt("What is your first name?");
let lname = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live?"),
	country: prompt("Which country does your city belong?")
}

let otherData = JSON.stringify({
	fname: fname,
	lname: lname,
	age: age,
	address: address	
})

console.log(otherData);
/*
let otherDataAgain = JSON.stringify({
fname: prompt("What is your first name?"),
lname: prompt("What is your last name?"),
age: prompt("What is your age?"),
address: {
	city: prompt("Which city do you live?"),
	country: prompt("Which country does your city belong?")
}
})
console.log(otherDataAgain);
*/
let batchesJSON = `[{"batchName": "Batch X"},{"batchName": "Batch Y"}]`;
console.log(batchesJSON);
console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));

console.log("Result from parse method");
console.log(JSON.parse(data));
console.log(JSON.parse(otherData));